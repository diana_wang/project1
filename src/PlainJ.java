

public class PlainJ {
	public double calPayment(double total, double discount) {
		
		return total *(1-discount);
	}

    public static void main(String[] args) {
        // Prints "Hello, World" to the terminal window.
    	double totalPrice = 100;
    	double discount = 0.15;
    	PlainJ myPlainJ = new PlainJ();
    	double paymentAmt = myPlainJ.calPayment(totalPrice, discount);
        System.out.println("Hello, World "+ paymentAmt);
    }
}
